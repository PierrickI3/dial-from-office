using System;
using System.Windows.Forms;
using System.IO;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;

using ININ.ThinClient.Resources;
using ININ.ThinClient.Common.Win32;
using ININ.Integrations.Windows.Base;

namespace DialFromWord
{
    public partial class ThisAddIn
    {
        #region Properties
        private Office.CommandBarButton btnDial;
        private bool m_bConnectedToClient = false;
        public bool ConnectedToClient
        {
            get
            {
                return m_bConnectedToClient;
            }
            set
            {
                using (TraceTopics.DialFromWordTrace.scope("ClientBHO.ConnectedToClient (set)"))
                {
                    if (value != m_bConnectedToClient)
                    {
                        m_bConnectedToClient = value;
                        try
                        {
                            TraceTopics.DialFromWordTrace.status("Setting the connected state to {}", m_bConnectedToClient);
                            ConnectedToClient = m_bConnectedToClient;
                        }
                        catch (System.Exception ex)
                        {
                            TraceTopics.DialFromWordTrace.exception(ex, "Exception caught in ClientBHO.ConnectedToClient (set)");
                        }
                    }
                }
            }
        }
        #endregion

        #region Addin Startup and Shutdown
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            using (TraceTopics.DialFromWordTrace.scope("ThisAddIn_Startup"))
            {
                try
                {
                    TraceTopics.DialFromWordTrace.note("Creating WindowBeforeRightClick event");
                    this.Application.WindowBeforeRightClick += new Microsoft.Office.Interop.Word.ApplicationEvents4_WindowBeforeRightClickEventHandler(Application_WindowBeforeRightClick);

                    RemoveDialButton();
                    CreateDialButton();
                    ProcessStartStopMonitoring();
                }
                catch(Exception ex)
                {
                    TraceTopics.DialFromWordTrace.exception(ex, "Exception caught in ThisAddIn_Startup");
                    MessageBox.Show("Exception in ThisAddIn_Startup " + ex.ToString());
                }
            }
        }
        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            using (TraceTopics.DialFromWordTrace.scope("ThisAddIn_Shutdown"))
            {
                try
                {
                    ClientCommunicationManager.Instance.ConnectionStateChanged -= new ClientCommunicationManager.ConnectionStateChangedDelegate(Instance_ConnectionStateChanged);
                    this.ConnectedToClient = false;
                    ClientCommunicationManager.Instance.RemoveClientBHO(this);
                }
                catch(Exception ex)
                {
                    TraceTopics.DialFromWordTrace.exception(ex, "Exception caught in ThisAddIn_Shutdown");
                    MessageBox.Show("Exception in ThisAddIn_Shutdown " + ex.ToString());
                }
            }
        }
        #endregion

        #region Connection Manager
        private void ProcessStartStopMonitoring()
        {
            using (TraceTopics.DialFromWordTrace.scope("ProcessStartStopMonitoring"))
            {
                try
                {
                    ClientCommunicationManager.Instance.AddClientBHO(this);
                    this.ConnectedToClient = ClientCommunicationManager.Instance.ConnectedToClient;

                    TraceTopics.DialFromWordTrace.status("Adding event connected state delegate");
                    ClientCommunicationManager.Instance.ConnectionStateChanged += new ClientCommunicationManager.ConnectionStateChangedDelegate(Instance_ConnectionStateChanged);
                }
                catch (System.Exception ex)
                {
                    TraceTopics.DialFromWordTrace.exception(ex, "Exception caught in ProcessStartStopMonitoring");
                }
            }
        }
        void Instance_ConnectionStateChanged(bool NewState)
        {
            using (TraceTopics.DialFromWordTrace.scope("Instance_ConnectionStateChanged"))
            {
                try
                {
                    TraceTopics.DialFromWordTrace.status("New connection state is {}", NewState);
                    this.ConnectedToClient = NewState;
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromWordTrace.exception(ex, "Exception caught in Instance_ConnectionStateChanged");
                    MessageBox.Show("Exception in Instance_ConnectionStateChanged " + ex.ToString());
                }
            }
        }
        #endregion

        #region Word Methods
        void Application_WindowBeforeRightClick(Microsoft.Office.Interop.Word.Selection Sel, ref bool Cancel)
        {
            using (TraceTopics.DialFromWordTrace.scope("Application_WindowBeforeRightClick"))
            {
                try
                {
                    Office.CommandBarButton DialButton = (Office.CommandBarButton)Application.CommandBars["Text"].FindControl(missing, missing, "DialFromOffice", missing, missing);
                    if (DialButton != null)
                    {
                        if (this.ConnectedToClient)
                        {
                            string sNumber = GetSelectedText();
                            TraceTopics.DialFromWordTrace.note("Number: " + sNumber);
                            if (sNumber != null)
                            {
                                DialButton.Enabled = true;
                                DialButton.Caption = string.Format(_3._0_Dial_From_Word.Properties.Strings.DIAL_PHONE_NUMBER, sNumber);
                                TraceTopics.DialFromWordTrace.note("Dial Button Caption: " + DialButton.Caption);
                            }
                            else
                            {
                                DialButton.Enabled = false;
                                DialButton.Caption = _3._0_Dial_From_Word.Properties.Strings.NO_PHONE_NUMBER_SELECTED;
                                TraceTopics.DialFromWordTrace.note("Dial Button Caption: " + DialButton.Caption);
                            }
                        }
                        else
                        {
                            DialButton.Enabled = false;
                            DialButton.Caption = _3._0_Dial_From_Word.Properties.Strings.CANNOT_DIAL_NOT_CONNECTED_TO_ANY_CLIENT;
                            TraceTopics.DialFromWordTrace.note("Dial Button Caption: " + _3._0_Dial_From_Word.Properties.Strings.CANNOT_DIAL_NOT_CONNECTED_TO_ANY_CLIENT);
                        }
                    }
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromWordTrace.exception(ex, "Exception in Application_WindowBeforeRightClick");
                    MessageBox.Show(ex.ToString(), "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void CreateDialButton()
        {
            using (TraceTopics.DialFromWordTrace.scope("CreateDialButton"))
            {
                try
                {
                    TraceTopics.DialFromWordTrace.note("Searching for the Dial Button");
                    btnDial = (Office.CommandBarButton)Application.CommandBars["Text"].Controls.Add(Office.MsoControlType.msoControlButton, missing, missing, missing, true);
                    if (btnDial == null)
                    {
                        TraceTopics.DialFromWordTrace.error("ERROR: new Dial Button was not created.");
                        return;
                    }
                    btnDial.Caption = "Dial";
                    btnDial.FaceId = 1982;
                    btnDial.Tag = "DialFromOffice";
                    btnDial.Enabled = true;
                    btnDial.Click += new Microsoft.Office.Core._CommandBarButtonEvents_ClickEventHandler(btnDial_Click);
                    TraceTopics.DialFromWordTrace.note("Dial Button successfully created.");
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromWordTrace.exception(ex, "Exception in CreateDialButton");
                    MessageBox.Show(ex.ToString(), "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void RemoveDialButton()
        {
            using (TraceTopics.DialFromWordTrace.scope("RemoveDialButton"))
            {
                try
                {
                    Office.CommandBarControl cbc = Application.CommandBars["Text"].FindControl(missing, missing, "DialFromOffice", missing, missing);
                    if (cbc != null)
                    {
                        TraceTopics.DialFromWordTrace.note("Dial menu entry already exists. Removing it...");
                        cbc.Delete(false);
                    }
                    else
                    {
                        TraceTopics.DialFromWordTrace.note("No Dial menu entry was found. Continuing...");
                    }
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromWordTrace.exception(ex, "Exception in RemoveDialButton");
                    MessageBox.Show(ex.ToString(), "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private string GetSelectedText()
        {
            using (TraceTopics.DialFromWordTrace.scope("GetSelectedText"))
            {
                try
                {
                    if (Application.Selection.Text.Trim().Length > 0)
                    {
                        if (Application.Selection.Words.Count > 1)
                        {
                            return Application.Selection.Text;
                        }
                        else
                        {
                            return Application.Selection.Words.First.Text;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromWordTrace.exception(ex, "Exception caught in ThisAddIn_Shutdown");
                    MessageBox.Show("Exception in ThisAddIn_Shutdown " + ex.ToString());
                    return null;
                }
            }
        }
        private void btnDial_Click(Microsoft.Office.Core.CommandBarButton Ctrl, ref bool CancelDefault)
        {
            using (TraceTopics.DialFromWordTrace.scope("btnDial_Click"))
            {
                try
                {
                    string sSelectedText = GetSelectedText();
                    TraceTopics.DialFromWordTrace.note("Connected to Client?: " + ClientCommunicationManager.Instance.ConnectedToClient.ToString());
                    TraceTopics.DialFromWordTrace.note("Selected Text: " + sSelectedText);

                    if (ClientCommunicationManager.Instance.ConnectedToClient && sSelectedText != null)
                    {
                        ININ.Integrations.CallControlBase ccb = ClientCommunicationManager.Instance.ClientCallControlBase;
                        if (ccb != null)
                        {
                            MakeCallAction mca = new MakeCallAction(ClientCommunicationManager.Instance.ClientCallControlBase);
                            mca.NumberToDial = sSelectedText;
                            TraceTopics.DialFromWordTrace.note("Calling: " + mca.NumberToDial);
                            mca.PerformAction();
                        }
                        else
                        {
                            TraceTopics.DialFromWordTrace.note("Showing: " + _3._0_Dial_From_Word.Properties.Strings.CANNOT_DIAL_NOT_CONNECTED_TO_ANY_CLIENT);
                            Ctrl.Caption = _3._0_Dial_From_Word.Properties.Strings.CANNOT_DIAL_NOT_CONNECTED_TO_ANY_CLIENT;
                        }
                    }
                    else
                    {
                        TraceTopics.DialFromWordTrace.note("Showing: " + _3._0_Dial_From_Word.Properties.Strings.CANNOT_DIAL_NOT_CONNECTED_TO_ANY_CLIENT);
                        Ctrl.Caption = _3._0_Dial_From_Word.Properties.Strings.CANNOT_DIAL_NOT_CONNECTED_TO_ANY_CLIENT;
                    }
                }
                catch (System.Exception ex)
                {
                    TraceTopics.DialFromWordTrace.exception(ex, "Exception caught in btnDial_Click");
                    MessageBox.Show(ex.ToString(), "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        #endregion
    }
}
