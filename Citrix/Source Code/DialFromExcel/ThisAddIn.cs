using System;
using System.Windows.Forms;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;

using ININ.ThinClient.Resources;
using ININ.ThinClient.Common.Win32;
using ININ.Integrations.Windows.Base;

namespace DialFromExcel
{
    public partial class ThisAddIn
    {
        #region Properties
        private Office.CommandBarButton btnDial;
        private bool m_bConnectedToClient = false;
        public bool ConnectedToClient
        {
            get
            {
                return m_bConnectedToClient;
            }
            set
            {
                using (TraceTopics.DialFromExcelTrace.scope("ClientBHO.ConnectedToClient (set)"))
                {
                    if (value != m_bConnectedToClient)
                    {
                        m_bConnectedToClient = value;
                        try
                        {
                            TraceTopics.DialFromExcelTrace.status("Setting the connected state to {}", m_bConnectedToClient);
                            ConnectedToClient = m_bConnectedToClient;
                        }
                        catch (System.Exception ex)
                        {
                            TraceTopics.DialFromExcelTrace.exception(ex, "Exception caught in ClientBHO.ConnectedToClient (set)");
                        }
                    }
                }
            }
        }
        #endregion

        #region Addin Startup and Shutdown
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            this.Application = (Excel.Application)Microsoft.Office.Tools.Excel.ExcelLocale1033Proxy.Wrap(typeof(Excel.Application), this.Application);
            using (TraceTopics.DialFromExcelTrace.scope("ThisAddIn_Startup"))
            {
                TraceTopics.DialFromExcelTrace.note("Creating SheetBeforeRightClick event");
                this.Application.SheetBeforeRightClick += new Microsoft.Office.Interop.Excel.AppEvents_SheetBeforeRightClickEventHandler(Application_SheetBeforeRightClick);
                RemoveDialButton();
                CreateDialButton();
                ProcessStartStopMonitoring();
            }
        }
        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            using (TraceTopics.DialFromExcelTrace.scope("ThisAddIn_Shutdown"))
            {
                try
                {
                    ClientCommunicationManager.Instance.ConnectionStateChanged -= new ClientCommunicationManager.ConnectionStateChangedDelegate(Instance_ConnectionStateChanged);
                    this.ConnectedToClient = false;
                    ClientCommunicationManager.Instance.RemoveClientBHO(this);
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromExcelTrace.exception(ex, "Exception caught in ThisAddIn_Shutdown");
                    MessageBox.Show("Exception in ThisAddIn_Shutdown " + ex.ToString());
                }
            }
        }
        #endregion

        #region Connection Manager
        private void ProcessStartStopMonitoring()
        {
            using (TraceTopics.DialFromExcelTrace.scope("ProcessStartStopMonitoring"))
            {
                try
                {
                    ClientCommunicationManager.Instance.AddClientBHO(this);
                    this.ConnectedToClient = ClientCommunicationManager.Instance.ConnectedToClient;

                    TraceTopics.DialFromExcelTrace.status("Adding event connected state delegate");
                    ClientCommunicationManager.Instance.ConnectionStateChanged += new ClientCommunicationManager.ConnectionStateChangedDelegate(Instance_ConnectionStateChanged);
                }
                catch (System.Exception ex)
                {
                    TraceTopics.DialFromExcelTrace.exception(ex, "Exception caught in ProcessStartStopMonitoring");
                }
            }
        }
        void Instance_ConnectionStateChanged(bool NewState)
        {
            using (TraceTopics.DialFromExcelTrace.scope("Instance_ConnectionStateChanged"))
            {
                TraceTopics.DialFromExcelTrace.status("New connection state is {}", NewState);
                this.ConnectedToClient = NewState;
            }
        }
        #endregion

        #region Excel Methods
        void Application_SheetBeforeRightClick(object Sh, Microsoft.Office.Interop.Excel.Range Target, ref bool Cancel)
        {
            using (TraceTopics.DialFromExcelTrace.scope("Application_WindowBeforeRightClick"))
            {
                try
                {
                    Office.CommandBarButton DialButton = (Office.CommandBarButton)Application.CommandBars["Cell"].FindControl(missing, missing, "DialFromOffice", missing, missing);
                    if (DialButton != null)
                    {
                        if (this.ConnectedToClient)
                        {
                            string sNumber = string.Empty;
                            if (Target.Text != null && Target.Text.ToString() != "")
                                sNumber = Target.Text.ToString();
                            else
                                sNumber = null;

                            if (sNumber != null)
                            {
                                TraceTopics.DialFromExcelTrace.note("Number: " + sNumber);
                                DialButton.Enabled = true;
                                DialButton.Caption = string.Format(Properties.Strings.DIAL_PHONE_NUMBER, sNumber);
                                TraceTopics.DialFromExcelTrace.note("Dial Button Caption: " + DialButton.Caption);
                            }
                            else
                            {
                                TraceTopics.DialFromExcelTrace.note("No text in selected range");
                                DialButton.Enabled = false;
                                DialButton.Caption = Properties.Strings.NO_PHONE_NUMBER_SELECTED;
                                TraceTopics.DialFromExcelTrace.note("Dial Button Caption: " + DialButton.Caption);
                            }
                        }
                        else
                        {
                            DialButton.Enabled = false;
                            DialButton.Caption = Properties.Strings.CANNOT_DIAL_WITH_NO_CLIENT;
                            TraceTopics.DialFromExcelTrace.note("Dial Button Caption: " + DialButton.Caption);
                        }
                    }
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromExcelTrace.exception(ex, "Exception in Application_WindowBeforeRightClick");
                    MessageBox.Show(ex.ToString(), "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void CreateDialButton()
        {
            using (TraceTopics.DialFromExcelTrace.scope("CreateDialButton"))
            {
                try
                {
                    TraceTopics.DialFromExcelTrace.note("Searching for the Dial Button");
                    btnDial = (Office.CommandBarButton)Application.CommandBars["Cell"].Controls.Add(Office.MsoControlType.msoControlButton, missing, missing, missing, true);
                    if (btnDial == null)
                    {
                        TraceTopics.DialFromExcelTrace.error("ERROR: new Dial Button was not created.");
                        return;
                    }
                    btnDial.Caption = "Dial";
                    btnDial.FaceId = 1982;
                    btnDial.Tag = "DialFromOffice";
                    btnDial.Enabled = true;
                    btnDial.Click += new Microsoft.Office.Core._CommandBarButtonEvents_ClickEventHandler(btnDial_Click);
                    TraceTopics.DialFromExcelTrace.note("Dial Button successfully created.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void RemoveDialButton()
        {
            using (TraceTopics.DialFromExcelTrace.scope("RemoveDialButton"))
            {
                try
                {
                    Office.CommandBarControl cbc = Application.CommandBars["Cell"].FindControl(missing, missing, "DialFromOffice", missing, missing);
                    if (cbc != null)
                    {
                        TraceTopics.DialFromExcelTrace.note("Dial menu entry already exists. Removing it...");
                        cbc.Delete(false);
                    }
                    else
                    {
                        TraceTopics.DialFromExcelTrace.note("No Dial menu entry was found. Continuing...");
                    }
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromExcelTrace.exception(ex, "Exception in RemoveDialButton");
                    MessageBox.Show(ex.ToString(), "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private string GetSelectedText()
        {
            if (Application.ActiveCell.Text != null && Application.ActiveCell.Text.ToString().Trim().Length > 0)
            {
                return Application.ActiveCell.Text.ToString().Trim();
            }
            else
            {
                return null;
            }
        }
        private void btnDial_Click(Microsoft.Office.Core.CommandBarButton Ctrl, ref bool CancelDefault)
        {
            using (TraceTopics.DialFromExcelTrace.scope("btnDial_Click"))
            {
                try
                {
                    string sSelectedText = GetSelectedText();
                    TraceTopics.DialFromExcelTrace.note("Connected to Client?: " + ClientCommunicationManager.Instance.ConnectedToClient.ToString());
                    TraceTopics.DialFromExcelTrace.note("Selected Text: " + sSelectedText);

                    if (ClientCommunicationManager.Instance.ConnectedToClient && sSelectedText != null)
                    {
                        ININ.Integrations.CallControlBase ccb = ClientCommunicationManager.Instance.ClientCallControlBase;
                        if (ccb != null)
                        {
                            MakeCallAction mca = new MakeCallAction(ClientCommunicationManager.Instance.ClientCallControlBase);
                            mca.NumberToDial = sSelectedText;
                            TraceTopics.DialFromExcelTrace.note("Calling: " + mca.NumberToDial);
                            mca.PerformAction();
                        }
                        else
                        {
                            TraceTopics.DialFromExcelTrace.note("Showing: " + Properties.Strings.CANNOT_DIAL_NOT_CONNECTED_TO_ANY_CLIENT);
                            Ctrl.Caption = Properties.Strings.CANNOT_DIAL_NOT_CONNECTED_TO_ANY_CLIENT;
                        }
                    }
                    else
                    {
                        TraceTopics.DialFromExcelTrace.note("Showing: " + Properties.Strings.CANNOT_DIAL_NOT_CONNECTED_TO_ANY_CLIENT);
                        Ctrl.Caption = Properties.Strings.CANNOT_DIAL_NOT_CONNECTED_TO_ANY_CLIENT;
                    }
                }
                catch (System.Exception ex)
                {
                    TraceTopics.DialFromExcelTrace.exception(ex, "Exception caught in btnDial_Click");
                    MessageBox.Show(ex.ToString(), "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        #endregion

    }
}
