using System;
using System.Collections.Generic;
using System.Text;

namespace DialFromExcel
{
    public class Tracing : TopicTracer
    {
        /// <exclude />
        public static int hdl = I3Trace.initialize_topic("Dial From Excel Plugin");
        /// <exclude />
        public override int get_handle() { return hdl; }
    }
    /// <exclude />
    public static class TraceTopics
    {
        static TraceTopics()
        {
            Init();
        }

        private static bool _Init = false;
        /// <exclude />
        public static void Init()
        {
            if (!_Init)
            {
                I3Trace.initialize_default_sinks();
                _Init = true;
            }
        }

        private static Tracing _DialFromExcelTrace = new Tracing();
        public static Tracing DialFromExcelTrace { get { return _DialFromExcelTrace; } }
    }
}
