﻿using System;
using System.Diagnostics;
using ININ.IceLib.Connection;
using ININ.IceLib.Interactions;

namespace DialNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            Session session = new Session();
            try
            {
                if (args.Length == 0)
                {
                    throw new Exception("No number passed");
                }
                Console.WriteLine("Dialing " + args[0]);
                StoredCredentials storedCredentials = CommonCredentials.GetCredentials();
                session.Connect(storedCredentials.SessionSettings, storedCredentials.HostSettings, storedCredentials.AuthSettings, storedCredentials.StationSettings);
                if (session.ConnectionState == ConnectionState.Up)
                {
                    InteractionsManager interactionsManager = InteractionsManager.GetInstance(session);
                    Interaction interaction = interactionsManager.MakeCall(new CallInteractionParameters(args[0]));
                }
            }
            catch (MakeCallException makeCallEx)
            {
                Debug.WriteLine("Call failed (wrong number?): " + makeCallEx.ToString());
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception in Main(): " + ex.ToString());
            }
            finally
            {
                if (session != null && session.ConnectionState == ConnectionState.Up)
                {
                    Debug.WriteLine("Disconnecting session");
                    session.DisconnectAsync(null, null);
                }
            }
        }
    }
}
