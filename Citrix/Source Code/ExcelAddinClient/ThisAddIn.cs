﻿using System;
using System.Windows.Forms;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using System.Diagnostics;
using ExcelAddinClient.Properties;
using Microsoft.Win32;
using System.IO;

namespace ExcelAddinClient
{
    public partial class ThisAddIn
    {
        private Office.CommandBarButton btnDial;

        #region Initialization and Shutdown
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            this.Application = (Excel.Application)Microsoft.Office.Tools.Excel.ExcelLocale1033Proxy.Wrap(typeof(Excel.Application), this.Application);
            this.Application.SheetBeforeRightClick += new Microsoft.Office.Interop.Excel.AppEvents_SheetBeforeRightClickEventHandler(Application_SheetBeforeRightClick);
            RemoveDialButton();
            CreateDialButton();
        }
        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }
        #endregion

        #region Excel Methods
        void Application_SheetBeforeRightClick(object Sh, Microsoft.Office.Interop.Excel.Range Target, ref bool Cancel)
        {
            try
            {
                Office.CommandBarButton DialButton = (Office.CommandBarButton)Application.CommandBars["Cell"].FindControl(missing, missing, "DialFromOffice", missing, missing);
                if (DialButton != null)
                {
                    string sNumber = string.Empty;
                    if (Target.Text != null && Target.Text.ToString() != "")
                        sNumber = Target.Text.ToString();
                    else
                        sNumber = null;

                    if (sNumber != null)
                    {

                        Debug.WriteLine("Number: " + sNumber);
                        DialButton.Enabled = true;
                        DialButton.Caption = string.Format(Resources.DIAL_PHONE_NUMBER, sNumber);
                        Debug.WriteLine("Dial Button Caption: " + DialButton.Caption);
                    }
                    else
                    {
                        Debug.WriteLine("No text in selected range");
                        DialButton.Enabled = false;
                        DialButton.Caption = Resources.NO_PHONE_NUMBER_SELECTED;
                        Debug.WriteLine("Dial Button Caption: " + DialButton.Caption);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception in Application_WindowBeforeRightClick: " + ex.ToString());
            }
        }
        private void CreateDialButton()
        {
            try
            {
                Debug.WriteLine("Searching for the Dial Button");
                btnDial = (Office.CommandBarButton)Application.CommandBars["Cell"].Controls.Add(Office.MsoControlType.msoControlButton, missing, missing, missing, true);
                if (btnDial == null)
                {
                    Debug.WriteLine("ERROR: new Dial Button was not created.");
                    return;
                }
                btnDial.Caption = "Dial";
                btnDial.FaceId = 1982;
                btnDial.Tag = "DialFromOffice";
                btnDial.Enabled = true;
                btnDial.Click += new Microsoft.Office.Core._CommandBarButtonEvents_ClickEventHandler(btnDial_Click);
                Debug.WriteLine("Dial Button successfully created.");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception in CreateDialButton: " + ex.ToString());
                MessageBox.Show(ex.ToString(), "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void RemoveDialButton()
        {
            try
            {
                Office.CommandBarControl cbc = Application.CommandBars["Cell"].FindControl(missing, missing, "DialFromOffice", missing, missing);
                if (cbc != null)
                {
                    Debug.WriteLine("Dial menu entry already exists. Removing it...");
                    cbc.Delete(false);
                }
                else
                {
                    Debug.WriteLine("No Dial menu entry was found. Continuing...");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception in RemoveDialButton: " + ex.ToString());
                MessageBox.Show(ex.ToString(), "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private string GetSelectedText()
        {
            if (Application.ActiveCell.Text != null && Application.ActiveCell.Text.ToString().Trim().Length > 0)
            {
                return Application.ActiveCell.Text.ToString().Trim();
            }
            else
            {
                return null;
            }
        }
        private void btnDial_Click(Microsoft.Office.Core.CommandBarButton Ctrl, ref bool CancelDefault)
        {
            bool numberDialed = false;

            try
            {
                string sSelectedText = GetSelectedText();
                Debug.WriteLine("Selected Text: " + sSelectedText);


                if (!String.IsNullOrEmpty(sSelectedText))
                {
                    if (System.Windows.Forms.SystemInformation.TerminalServerSession)
                    {
                        Debug.WriteLine("Current session is a terminal server session");
                        Process[] icProcesses = Process.GetProcessesByName("InteractionClient");
                        if (icProcesses != null && icProcesses.Length > 0)
                        {
                            Debug.WriteLine("Interaction Client is running. Run DialNumber.");

                            //Trying to get path with process info
                            string clientPath = icProcesses[0].MainModule.FileName.Substring(0, icProcesses[0].MainModule.FileName.LastIndexOf("\\") + 1);
                            Debug.WriteLine("Client Path: " + clientPath);
                            if (!String.IsNullOrEmpty(clientPath))
                            {
                                string dialNumberPath = clientPath.ToString() + "DialNumber.exe";
                                if (File.Exists(dialNumberPath))
                                {
                                    Debug.WriteLine("DialNumber found. Calling " + dialNumberPath + " " + sSelectedText);
                                    Process.Start(new ProcessStartInfo(dialNumberPath, sSelectedText));
                                    numberDialed = true;
                                }
                            }
                            else
                            {
                                // If it didn't work, try with registry information
                                RegistryKey key = Registry.LocalMachine;
                                RegistryKey clientKey = key.OpenSubKey("SOFTWARE\\Interactive Intelligence\\Installed\\Interaction Client .Net Edition");
                                object installDir = clientKey.GetValue("InstallDir", "C:\\Program Files\\Interactive Intelligence\\ICUserApps\\");
                                if (installDir != null)
                                {
                                    Debug.WriteLine("Found client in " + installDir.ToString());
                                    string dialNumberPath = installDir.ToString() + "DialNumber.exe";
                                    if (File.Exists(dialNumberPath))
                                    {
                                        Debug.WriteLine("DialNumber found. Calling " + dialNumberPath + " " + sSelectedText);
                                        Process.Start(new ProcessStartInfo(dialNumberPath, sSelectedText));
                                        numberDialed = true;
                                    }
                                }
                                else
                                {
                                    Debug.WriteLine("Could not find client path");
                                }
                            }
                        }
                    }
                    if (!numberDialed)
                    {
                        Process.Start("callto:" + sSelectedText);
                    }
                }
                else
                {
                    Ctrl.Caption = Resources.NO_PHONE_NUMBER_SELECTED;
                }
            }
            catch (System.Exception ex)
            {
                Debug.WriteLine("Exception caught in btnDial_Click: " + ex.ToString());
                MessageBox.Show(ex.ToString(), "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion
    }
}
