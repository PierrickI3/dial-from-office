﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using WordAddinClient.Properties;
using Office = Microsoft.Office.Core;
using Microsoft.Win32;
using System.IO;

namespace WordAddinClient
{
    public partial class ThisAddIn
    {
        #region Properties
        private Office.CommandBarButton btnDial;
        #endregion

        #region Initialization and Shutdown
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            Debug.WriteLine("Running Internal Startup()");

            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Dial From Word add-in starting");
            try
            {
                Debug.WriteLine("Creating WindowBeforeRightClick event");
                this.Application.WindowBeforeRightClick += new Microsoft.Office.Interop.Word.ApplicationEvents4_WindowBeforeRightClickEventHandler(Application_WindowBeforeRightClick);

                RemoveDialButton();
                CreateDialButton();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception in ThisAddIn_Startup " + ex.ToString());
            }
        }
        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Addin shutting down");
        }
        #endregion

        #region Word Methods
        void Application_WindowBeforeRightClick(Microsoft.Office.Interop.Word.Selection Sel, ref bool Cancel)
        {
            try
            {
                Office.CommandBarButton DialButton = (Office.CommandBarButton)Application.CommandBars["Text"].FindControl(missing, missing, "DialFromOffice", missing, missing);
                if (DialButton != null)
                {
                    string sNumber = GetSelectedText();
                    Debug.WriteLine("Number: " + sNumber);
                    if (sNumber != null)
                    {
                        DialButton.Enabled = true;
                        DialButton.Caption = string.Format(Resources.DIAL_PHONE_NUMBER, sNumber);
                        Debug.WriteLine("Dial Button Caption: " + DialButton.Caption);
                    }
                    else
                    {
                        DialButton.Enabled = false;
                        DialButton.Caption = Resources.NO_PHONE_NUMBER_SELECTED;
                        Debug.WriteLine("Dial Button Caption: " + DialButton.Caption);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception in Application_WindowBeforeRightClick " + ex.ToString());
            }
        }
        private void CreateDialButton()
        {
            try
            {
                Debug.WriteLine("Searching for the Dial Button");
                btnDial = (Office.CommandBarButton)Application.CommandBars["Text"].Controls.Add(Office.MsoControlType.msoControlButton, missing, missing, missing, true);
                if (btnDial == null)
                {
                    Debug.WriteLine("ERROR: new Dial Button was not created.");
                    return;
                }
                btnDial.Caption = "Dial";
                btnDial.FaceId = 1982;
                btnDial.Tag = "DialFromOffice";
                btnDial.Enabled = true;
                btnDial.Click += new Microsoft.Office.Core._CommandBarButtonEvents_ClickEventHandler(btnDial_Click);
                Debug.WriteLine("Dial Button successfully created.");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception in CreateDialButton " + ex.ToString());
            }
        }
        private void RemoveDialButton()
        {
            try
            {
                Office.CommandBarControl cbc = Application.CommandBars["Text"].FindControl(missing, missing, "DialFromOffice", missing, missing);
                if (cbc != null)
                {
                    Debug.WriteLine("Dial menu entry already exists. Removing it...");
                    cbc.Delete(false);
                }
                else
                {
                    Debug.WriteLine("No Dial menu entry was found. Continuing...");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception in RemoveDialButton " + ex.ToString());
            }
        }
        private string GetSelectedText()
        {
            try
            {
                if (Application.Selection.Text.Trim().Length > 0)
                {
                    if (Application.Selection.Words.Count > 1)
                    {
                        return Application.Selection.Text;
                    }
                    else
                    {
                        return Application.Selection.Words.First.Text;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception in GetSelectedText " + ex.ToString());
                return null;
            }
        }
        private void btnDial_Click(Microsoft.Office.Core.CommandBarButton Ctrl, ref bool CancelDefault)
        {
            bool numberDialed = false;
            try
            {
                string sSelectedText = GetSelectedText();
                Debug.WriteLine("Selected Text: " + sSelectedText);
                if (!String.IsNullOrEmpty(sSelectedText))
                {
                    if (System.Windows.Forms.SystemInformation.TerminalServerSession)
                    {
                        Debug.WriteLine("Current session is a terminal server session");
                        Process[] icProcesses = Process.GetProcessesByName("InteractionClient");
                        if (icProcesses != null && icProcesses.Length > 0)
                        {
                            Debug.WriteLine("Interaction Client is running. Run DialNumber.");
                         
                            //Trying to get path with process info
                            string clientPath = icProcesses[0].MainModule.FileName.Substring(0, icProcesses[0].MainModule.FileName.LastIndexOf("\\") + 1);
                            Debug.WriteLine("Client Path: " + clientPath);
                            if (!String.IsNullOrEmpty(clientPath))
                            {
                                string dialNumberPath = clientPath.ToString() + "DialNumber.exe";
                                if (File.Exists(dialNumberPath))
                                {
                                    Debug.WriteLine("DialNumber found. Calling " + dialNumberPath + " " + sSelectedText);
                                    Process.Start(new ProcessStartInfo(dialNumberPath, sSelectedText));
                                    numberDialed = true;
                                }
                            }
                            else
                            {
                                // If it didn't work, try with registry information
                                RegistryKey key = Registry.LocalMachine;
                                RegistryKey clientKey = key.OpenSubKey("SOFTWARE\\Interactive Intelligence\\Installed\\Interaction Client .Net Edition");
                                object installDir = clientKey.GetValue("InstallDir", "C:\\Program Files\\Interactive Intelligence\\ICUserApps\\");
                                if (installDir != null)
                                {
                                    Debug.WriteLine("Found client in " + installDir.ToString());
                                    string dialNumberPath = installDir.ToString() + "DialNumber.exe";
                                    if (File.Exists(dialNumberPath))
                                    {
                                        Debug.WriteLine("DialNumber found. Calling " + dialNumberPath + " " + sSelectedText);
                                        Process.Start(new ProcessStartInfo(dialNumberPath, sSelectedText));
                                        numberDialed = true;
                                    }
                                }
                                else
                                {
                                    Debug.WriteLine("Could not find client path");
                                }
                            }
                        }
                    }
                    if (!numberDialed)
                    {
                        Process.Start("callto:" + sSelectedText);
                    }
                }
                else
                {
                    Debug.WriteLine(Resources.NO_PHONE_NUMBER_SELECTED);
                }
            }
            catch (System.Exception ex)
            {
                Debug.WriteLine("Exception in btnDial_Click: " + ex.ToString());
            }
        }
        #endregion
    }
}
