# Dial from Office #

Find the documentation [here](https://bitbucket.org/PierrickI3/dial-from-office/raw/5f3bd07a7f2b36e770b45092ab3271320e6f43f9/Documentation/Dial%20from%20Office%20plug-ins.pdf)


This repository contains the source code for the Dial from Office (Word/Excel) plug-in as well as the Citrix version. Please look at the [source code](https://bitbucket.org/PierrickI3/dial-from-office/src) for more information.