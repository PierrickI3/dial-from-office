using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ININ.Integrations.Windows.Base;
using ININ.ThinClient.Common;
using ININ.ThinClient.Common.Win32;

namespace DialFromWord
{
    internal class ClientCommunicationManager : ININ.Integrations.ICallControlImpl, IDisposable
    {
        public static ClientCommunicationManager Instance
        {
            get
            {
                lock (s_syncObject)
                {
                    if (s_instance == null)
                    {
                        s_instance = new ClientCommunicationManager();
                    }
                }
                return s_instance;
            }
        }
        private static Object s_syncObject = "Locker";
        private static ClientCommunicationManager s_instance = null;

        public delegate void ConnectionStateChangedDelegate(bool NewState);
        public event ConnectionStateChangedDelegate ConnectionStateChanged;

        public ININ.Integrations.CallControlBase ClientCallControlBase
        {
            get
            {
                return m_callControl;
            }
        }

        #region BrowserHelperObjects that we're monitoring.
        public bool AddClientBHO(ThisAddIn bho)
        {
            using (TraceTopics.DialFromWordTrace.scope("ClientCommunicationManager.AddClientBHO"))
            {
                bool bRet = false;
                try
                {
                    if (bho != null)
                    {
                        lock (m_clientBHOsToMonitor)
                        {
                            if (m_clientBHOsToMonitor.Contains(bho) == false)
                            {
                                m_clientBHOsToMonitor.Add(bho);
                            }
                            if (m_callControl == null)
                            {
                                TraceTopics.DialFromWordTrace.status("Creating CallControlBase instance.");
                                m_callControl = new ININ.Integrations.CallControlBase(this);
                            }

                            bRet = true;
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    TraceTopics.DialFromWordTrace.exception(ex, "Exception caught in ClientCommunicationManager.AddClientBHO");
                }
                TraceTopics.DialFromWordTrace.verbose("Return: {}", bRet);
                return bRet;
            }
        }

        public bool RemoveClientBHO(ThisAddIn cbho)
        {
            using (TraceTopics.DialFromWordTrace.scope("ClientCommunicationManager.RemoveClientBHO"))
            {
                bool bRet = false;
                try
                {
                    if (cbho != null)
                    {
                        lock (m_clientBHOsToMonitor)
                        {
                            if (m_clientBHOsToMonitor.Contains(cbho) == true)
                            {
                                m_clientBHOsToMonitor.Remove(cbho);
                            }
                            if (m_clientBHOsToMonitor.Count == 0)
                            {
                                if (m_callControl != null)
                                {
                                    IDisposable DisposableCallControl = (IDisposable)m_callControl;
                                    if (DisposableCallControl != null)
                                    {
                                        TraceTopics.DialFromWordTrace.status("Disposing CallControlBase instance.");
                                        DisposableCallControl.Dispose();
                                        m_callControl = null;
                                    }
                                }
                            }
                            bRet = true;
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    TraceTopics.DialFromWordTrace.exception(ex, "Exception caught in ClientCommunicationManager.RemoveClientBHO");
                }
                TraceTopics.DialFromWordTrace.verbose("Return: {}", bRet);
                return bRet;
            }
        }

        public bool StopMonitoringAllClientBHOs()
        {
            using (TraceTopics.DialFromWordTrace.scope("ClientCommunicationManager.StopMonitoringAllClientBHOs"))
            {
                bool bRet = false;
                try
                {
                    lock (m_clientBHOsToMonitor)
                    {
                        m_clientBHOsToMonitor.Clear();
                    }
                    bRet = true;
                }
                catch (System.Exception ex)
                {
                    TraceTopics.DialFromWordTrace.exception(ex, "Exception caught in ClientCommunicationManager.StopMonitoringAllClientBHOs");
                }
                TraceTopics.DialFromWordTrace.verbose("Return: {}", bRet);
                return bRet;
            }

        }

        /// <summary>
        /// Returns the currently active ClientBrowserHelperObject
        /// </summary>
        public ThisAddIn ActiveClientBrowserHelperObject
        {
            get
            {
                lock (m_clientBHOsToMonitor)
                {
                    foreach (ThisAddIn cbho in m_clientBHOsToMonitor)
                    {
                        if ((cbho.Application != null))
                        {
                            return cbho;
                        }
                    }
                }
                return null;
            }
        }

        private List<ThisAddIn> m_clientBHOsToMonitor = new List<ThisAddIn>();
        #endregion

        private ININ.Integrations.CallControlBase m_callControl = null;

        #region ICallControlImpl Members
        public string GetAppName()
        {
            using (TraceTopics.DialFromWordTrace.scope("GetAppName"))
            {
                string ProcName = ProcessInfo.GetProcessName();
                TraceTopics.DialFromWordTrace.verbose("Return: {}", ProcName);
                return ProcName;
            }
        }

        public string[] GetWatchedAttributes()
        {
            return new string[0];
        }

        public void PerformCustomAction(string p_Command, ININ.ThinClient.ClientLib.Session p_Session)
        {
        }

        public bool PopInteraction(ININ.ThinClient.ClientLib.Session p_Session, ININ.ThinClient.Interaction p_Interaction)
        {
            using (TraceTopics.DialFromWordTrace.scope("ClientBrowserHelperObjectManager.PopInteraction"))
            {
                TraceTopics.DialFromWordTrace.verbose("Return: {}", false);
                return false;
            }
        }

        public void UpdateButtonState(ININ.Integrations.ButtonStateParams p_Params)
        {
        }

        public void UpdateButtonVisibility(int p_Capabilities)
        {
        }

        public void UpdateCallerID(string p_RemoteID, string p_RemoteName)
        {
        }

        public void UpdateLoginInfo(bool p_Connected, ININ.ThinClient.ClientLib.Session p_Session)
        {
            using (TraceTopics.DialFromWordTrace.scope("ClientBrowserHelperObjectManager.UpdateLoginInfo"))
            {
                if (m_bConnectedToClient != p_Connected)
                {
                    m_bConnectedToClient = p_Connected;
                    if (ConnectionStateChanged != null)
                    {
                        try
                        {
                            ConnectionStateChanged(m_bConnectedToClient);
                        }
                        catch (System.Exception ex)
                        {
                            TraceTopics.DialFromWordTrace.exception(ex, "Exception caught firing connection state changed event.");
                        }
                    }
                }
            }
        }

        public bool ConnectedToClient
        {
            get
            {
                return m_bConnectedToClient;
            }
        }
        private bool m_bConnectedToClient = false;

        #endregion

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            m_callControl = null;
        }

        #endregion

    }

}
